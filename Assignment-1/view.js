let url = 'https://jsonplaceholder.typicode.com/users';
let table = document.getElementById('people');
async function retrieve() {
    let response = await fetch(url);
    let json = await response.json();
    for (let i = 0; i < json.length; i++) {
        let obj = json[i];
        let temp = [obj.name,obj.email,obj.phone,obj.website,obj.company.name];
        let row = document.createElement('tr');
        row.setAttribute("class","row");
        row.setAttribute("number",obj.id);
        row.setAttribute("uname",obj.name);
        for(let j=0;j<temp.length;j++)
        {
            let data = document.createElement('td');
            data.appendChild(document.createTextNode(temp[j]));
            row.appendChild(data);
        }
       table.appendChild(row);
    }
    let rows = document.querySelectorAll(".row");
    rows.forEach((row)=>{row.addEventListener('click',()=>{
        // console.log(row.getAttribute("uname"))
    window.location = `post1.html?id=${row.getAttribute("number")}&name=${row.getAttribute("uname")}`;
   });
});
}

retrieve();







