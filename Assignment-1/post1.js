let flag = false,posts_cnt;
function retrieve_Userid(){
    let id = window.location.search, Name;
    id = id.split("?")[1];
    Name =  id.split("&")[1];
    id = id.split("&")[0];
    id = id.split("=")[1];
    Name = Name.split("=")[1];
    document.getElementById("head").innerHTML = Name.split("%20")[0]+" "+Name.split("%20")[1];
    return id;
}

function delete_postcard(){
    let del_list = document.querySelectorAll(".del");
    del_list.forEach((el) => {
        el.style.cursor = "pointer";
       
        el.onclick =  (event) => {
            
            let temp1 = document.querySelector(`[postId="${el.getAttribute("delId")}"`);
            console.log(temp1);
            if(confirm("Do you want to delete?")){
            temp1.remove();
            // temp1.style.display = "none";
            posts_cnt--;
            }
            flag=true;
            if(posts_cnt==0)
            {
                document.getElementById("fallback_text").style.display = "block";
                
            }
            
        };
    });

}
function multi_delete(){
    let del_all = document.getElementsByClassName("del-unv")[0];
    del_all.style.cursor = "pointer";
    del_all.onclick = ()=>{
        let checked_list = document.querySelectorAll(".chk:checked");
        if(checked_list.length!=0)
        {
            if(confirm("Do you want to delete?"))
            {
                posts_cnt-=checked_list.length;
                // console.log(posts_cnt);
                if(posts_cnt==0)
                {
                    document.getElementById("fallback_text").style.display = "block";
                    document.getElementsByClassName("del-unv")[0].checked = false;
                }
                checked_list.forEach((el)=>{
                    let temp = document.querySelector(`[postId="${el.getAttribute("chkId")}"]`);
                    temp.remove();
                    // temp.style.display = "none";
                    
                });
            }
        }
    };
}

function select_all(){
    let chk_box = document.getElementsByClassName("chk-unv")[0];
    chk_box.addEventListener('click',()=>{
        let chk_list =  document.querySelectorAll("[chkId]");
        if(chk_box.checked)
        {
            
            chk_list.forEach((el)=>{
                el.checked = true;
                document.querySelector(`[postId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "lightskyblue";
                document.querySelector(`[delId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "lightskyblue";
                document.querySelector(`[editId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "lightskyblue";      
            });
        }
        else
        {
            
            chk_list.forEach((el)=>{
                el.checked = false;
                document.querySelector(`[postId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "rgb(177, 248, 215)";
                document.querySelector(`[delId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "rgb(177, 248, 215)";
                document.querySelector(`[editId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "rgb(177, 248, 215)"; 
            });
        }
    });
}

async function create(id) 
{
    let posts = await fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`);
    let data = await posts.json();
    posts_cnt = data.length;
    let comments_res = await fetch(`https://jsonplaceholder.typicode.com/comments`);
    let comments = await comments_res.json();
    // console.log(comments);
    for (let i = 0; i < data.length; i++) {
        let top_comments = [comments[i*5],comments[i*5+1],comments[i*5+2]];
    // let comment = await fetch(`https://jsonplaceholder.typicode.com/comments?postId=${data[i].id}`);
    // let data1 = await comment.json();
    let code = `
        <div class="post" postId=${i}>
        <label>
        <div class="menu">
            <div class="chk-div"><input type="checkbox" class="chk" chkId=${i}>Select</div>
            <button class="fa fa-trash del" delId=${i}></button>
            <button class="fa fa-edit edit" editId=${i}></button>
        </div>
        <div class="Title">
            <span class="key">Title : </span>
            <span class="value">${data[i].title}</span>
        </div>
        <div class="Body">
            <span class="key">Body : </span>
            <span class="value">${data[i].body}</span>
        </div>
        <div class="comment-box">
            <p class="key">Comments : </p>
            <div class="comment">
                <div class="cmt-name">
                    <span class="key">Name : </span>
                    <span class="value"> ${top_comments[0].name}</span>
                </div>
                <div class="cmt_body">
                    <span class="key">Body : </span>
                    <span class="value">${top_comments[0].body}</span>
                </div>
                <div class="cmt-email">
                    <span class="key">Email : </span>
                    <span class="value">${top_comments[0].email}</span>
                </div>
            </div>
            <div class="comment">
                
            <div class="cmt-name">
            <span class="key">Name : </span>
            <span class="value"> ${top_comments[1].name}</span>
        </div>
        <div class="cmt_body">
            <span class="key">Body : </span>
            <span class="value">${top_comments[1].body}</span>
        </div>
        <div class="cmt-email">
            <span class="key">Email : </span>
            <span class="value">${top_comments[1].email}</span>
        </div>
            </div>
            <div class="comment">
            <div class="cmt-name">
            <span class="key">Name : </span>
            <span class="value"> ${top_comments[2].name}</span>
        </div>
        <div class="cmt_body">
            <span class="key">Body : </span>
            <span class="value">${top_comments[2].body}</span>
        </div>
        <div class="cmt-email">
            <span class="key">Email : </span>
            <span class="value">${top_comments[2].email}</span>
        </div>
            </div>
        </div>
        </label>
    </div>`;
    document.getElementsByClassName("container")[0].innerHTML += code;
    }

    
    //adding event listener to each checkbox
    let chk_list = document.querySelectorAll('.chk');
    chk_list.forEach((el)=>{
        el.addEventListener('input',()=>{
            
            if(el.checked)
            {
                document.querySelector(`[editId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "lightskyblue";
                document.querySelector(`[delId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "lightskyblue";
                document.querySelector(`[postId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "lightskyblue"; 
            }
            else
            {
                
                document.querySelector(`[editId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "rgb(177, 248, 215)";
                document.querySelector(`[delId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "rgb(177, 248, 215)";
                document.querySelector(`[postId="${el.getAttribute("chkId")}"]`).style.backgroundColor = "rgb(177, 248, 215)";

            }
        })
    })

    //deleting each post card
    delete_postcard();
    


    //Multiple delete 
    multi_delete();
    

    //Select All
    select_all();
}
let UserId = retrieve_Userid();
create(UserId);
